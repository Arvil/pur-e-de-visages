<%-- 
    Document   : updateInfo
    Created on : 2 janv. 2013, 12:16:31
    Author     : mbit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       
         <title>Éditer infos</title>
        <c:url value='/bootstrap/css/bootstrap.min.css' var='link2css'/>
        <link type="text/css" rel="stylesheet" href="${ link2css }" />
        <link rel="shortcut icon" type="image/x-icon" href="<c:url value="/img/favicon.png" />"/>

    </head>
    <body>
        <%-- gestion des url --%>
        <c:url value='../inc/inc_navbar.jsp' var='link2navbar'/>
        <c:url value='../inc/footer.jsp' var='link2footer'/>
        <c:url value='../inc/javascript.jsp' var='link2script'/>
        <c:url value='../inc/inc_updateInfo_form.jsp' var='link2updateForm'/>
        <c:url value='/UpdateInfo' var='link2update'/>
        
  
        
         <div class="container">            
            <c:import url="${ link2navbar }" /> 
            <div class="row">
                <div class="span6 offset 1">
                    <form class="well form-vertical" method="post" action="${ link2update }" id="formulaire">
                        <input type="hidden" name="action" value="form_update" />
                        <fieldset>
                            <legend>Mise à jour de mes informations</legend>                           
                            <c:import url="${ link2updateForm }"/>                           
                        </fieldset>
                    </form>
                </div>                
            </div>
        
          <c:import url="${ link2footer }"/> 
                            
          </div>
    
                <c:import url="${ link2script }"/>
    <script>
         $(function(){
             //fermer les messages d'erreur après 3 secondes
             $("div.alert").show("slow").delay(3000).hide("slow");
             $("span.label.label-info").show("slow").delay(3000).hide("slow");
             $("span.label.label-success").show("slow").delay(3000).hide("slow");
             $("span.label.label-warning").show("slow").delay(3000).hide("slow");
             
             
         });
    </script>
    </body>
</html>
