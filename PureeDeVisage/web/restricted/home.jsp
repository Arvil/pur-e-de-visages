<%-- 
    Document   : page1
    Created on : 29 déc. 2012, 23:24:26
    Author     : mbit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <c:url value='/bootstrap/css/bootstrap.min.css' var='link2css'/>
        <link type="text/css" rel="stylesheet" href="${ link2css }" />  
        <link rel="shortcut icon" type="image/x-icon" href="<c:url value="/img/favicon.png" />"/>

    </head>
    
    <body>
        <div class="container">
            <c:import url="../inc/inc_navbar.jsp"/>
             
            <header class="page-header hero-unit">
                <h1> Mon Espace personnel </h1>
            
            </header>
                      <p>
                          Bonjour ${sessionScope.userSession.pseudo}. Vous êtes connecté(e) avec l'adresse ${sessionScope.userSession.email}
                      </p>
            <h3>Mes images</h3>          
            <c:if test="${empty images}">
                <p>
                 Vous n'avez uploadez aucune image.
                </p>
            </c:if>  
            <c:if test="${!empty images}">                
            <div class="row">
               <div id="content">
                   <c:forEach items="${images}" var="news">
                       <c:url value="/DisplayImage" var="img"> 
                              <c:param name="ImageName" value="${news.picturePath}"/>
                       </c:url>                      
                       
                       <li class="span2">
                           <div class="thumbnail"> 
                               <a href="<c:url value="/PureeDeVisage"><c:param name="idphoto" value="${news.id}"/></c:url>" data-toggle="modal">
                              <img src="${img}" alt="${news.firstName}" title="${news.lastName}" style="width: 150px; height: 120px"/>
                            </a>
                             
                           </div>
                       </li> 
                     </c:forEach>   
               </div>
            </div>
            <div id="page_navigation"> </div>
            </c:if>
            
            <c:if test="${!empty face}">
                <c:import url="../inc/inc_displayface.jsp"/>
             </c:if>
            
            <div class="row">
                <div class="span7 well">
                    <h3>Statistiques</h3>
                    <c:import url="/inc/inc_statistiques.jsp"/>
                </div>
            </div>
            
                  
            <c:import url="../inc/footer.jsp"/>
        </div>      
        
        <%-- SCRIPT --%>
        <c:import url="../inc/javascript.jsp"/>
        <script src="<c:url value="/inc/paginationScript.js"/>"></script>
    </body>
</html>
