<%-- 
    Document   : inscription
    Created on : 28 d�c. 2012, 17:28:09
    Author     : mbit
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Cr�ation visage</title>
        <c:url value='/bootstrap/css/bootstrap.min.css' var='link2css'/>
        <link type="text/css" rel="stylesheet" href="${ link2css }" />
        <link rel="shortcut icon" type="image/x-icon" href="<c:url value="/img/favicon.png" />"/>

    </head>

    <body>
        
        <%-- gestion des url --%>
        <c:url value='../inc/inc_navbar.jsp' var='link2navbar'/>
        <c:url value='../inc/footer.jsp' var='link2footer'/>
        <c:url value='../inc/javascript.jsp' var='link2script'/>
        <c:url value='../inc/inc_createVisage_form.jsp' var='link2visageForm'/>
        <c:url value='/VisageCreation' var='link2VisageCreation'/>

        <div class="container">            
            <c:import url="${ link2navbar }" /> 
            <div class="row">
                <div class="span6">
                    <form class="well form-vertical" method="post" action="${ link2VisageCreation }" enctype="multipart/form-data" id="formulaire">                        
                        <fieldset>
                            <legend>Ajout d'un Visage</legend>                           
                            <c:import url="${ link2visageForm }"/>
                        </fieldset>
                    </form>
                </div> 
                <div class="span6">
                    <img src="<c:url value="/img/visage.png"/>" alt="visage"/>
                </div>
            </div>
          
                <c:import url="${ link2footer }"/> 
                            
          </div>
    
                <c:import url="${ link2script }"/>
    <script>
         $(function(){
             //fermer les messages d'erreur apr�s 3 secondes
             $("div.alert").show("slow").delay(3000).hide("slow");
             $("span.label.label-info").show("slow").delay(3000).hide("slow");
             $("span.label.label-success").show("slow").delay(3000).hide("slow");
             $("span.label.label-warning").show("slow").delay(3000).hide("slow");
             $('.fileupload').fileupload();
             
         });
    </script>
               
    </body> 

</html>
