<%-- 
    Document   : index
    Created on : 1 janv. 2013, 12:54:12
    Author     : arvil
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Accueil</title>
        <link rel="shortcut icon" type="image/x-icon" href="<c:url value="/img/favicon.png" />"/>
        <c:url value='/bootstrap/css/bootstrap.min.css' var='link2css'/>
        <link type="text/css" rel="stylesheet" href="${ link2css }" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/bootstrap/css/bootstrap-image-gallery.min.css"/>"  >
        <link href="<c:url value="/bootstrap/css/animate.css"/>" rel="stylesheet" type="text/css" > 
    </head>
    <body>
        <div class="container">
             <c:import url="inc/inc_navbar.jsp"/>
             <c:import url="inc/header.jsp"/>
              <div class="row">
		<div class="span12"> 
		    <p>
		    La passion pour les <strong>visages</strong> date de très longtemps. Ce site a été construit en <em>hommage à ces merveilleux visages...</em><br>
		    Nous faisons partie de la <abbr title="Purée de visage">PDV</abbr> qui a pour but de noter ces splendides visages féminins.
		    </p>
		    <p>
		    Voici ce qu'en dit Wikipédia:
		    </p>
		    <blockquote>
                        Le visage est la zone externe de la partie antérieure du crâne de l'être humain, appelée aussi face ou figure. Il se structure autour de zones osseuses abritant plusieurs organes des sens ; il comprend notamment la peau, le menton, la bouche, les lèvres, le philtrum, 
                        les dents, le nez, les joues, les yeux, les sourcils, le front, les cheveux et les oreilles.
Chaque visage est unique. Il est un élément essentiel de l'identité ; il sert à ce titre à l'identification des personnes .<br>
			<small class="pull-right"><a id="tool" href="http://wikipedia.org" data-original-title="Acceder à la page">Wikipedia</a></small><br />
		    </blockquote>
		</div>
	    </div>         
      
  
               
	    <div class="row">
		<div class="span12">		   
		     <div>
                          <ul class="thumbnails">
                              <c:forEach begin="1" end="12" step="1" var="i">
                                  <c:url value="/img/img${i}.jpg" var="imgi"/>                                   
                                  <li class="span2"> <div class="thumbnail"><a href="${imgi}" title="visage ${i}"><img src="${imgi}" alt="visage" style="width: 150px; height: 120px"></a> <h3>hllh</h3><p>description</p></div></li>
                              </c:forEach>                             
                          </ul>
                      </div>  			   
		    
		</div>
	    </div>
              
        
              
             <c:import url="inc/footer.jsp"/>
        </div>
             <c:import url="inc/javascript.jsp"/>
             
             <script>
                 $(function(){
                        $('#tool').tooltip({placement:'top'});
                        $('.carousel').carousel('cycle');
                 });
              </script>
    </body>
</html>
