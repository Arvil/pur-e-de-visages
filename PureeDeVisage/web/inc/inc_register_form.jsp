<%-- 
    Document   : inc_register_form
    Created on : 30 d�c. 2012, 08:49:06
    Author     : mbit
--%>

<label for="email">Adresse email </label>
<input type="email" id="email" name="email" placeholder="Email" required value="<c:out value="${user.email}"/>" size="20" maxlength="60" />
<c:if test="${!empty form.errors.email}">
    <div class="alert alert-error">
        <button type="button" class="close" >&times;</button>
         ${form.errors.email} 
    </div>
</c:if>


<label for="motdepasse">Mot de passe</label>
<input type="password" id="motdepasse" name="motdepasse" placeholder="Mot de passe" required value="" size="20" maxlength="20" />

<label for="confirmation">Confirmation du mot de passe </label>
<input type="password" id="confirmation" required name="confirmation" placeholder="Confirmation mot de passe" value="" size="20" maxlength="20" />

<c:if test="${!empty form.errors.motdepasse}">
    <div class="alert alert-error">
        <button type="button" class="close" >&times;</button>
         ${form.errors.motdepasse} 
    </div>
</c:if>

<label for="nom">Nom d'utilisateur</label>
<input type="text" id="nom" required name="nom" placeholder="Pseudo" value="<c:out value="${user.pseudo}"/>" size="20" maxlength="20" />

<c:if test="${!empty form.errors.nom}">
    <div class="alert alert-error">
        <button type="button" class="close" >&times;</button>
         ${form.errors.nom}
    </div>
</c:if>

<h5>Sexe</h5>

<label for="homme" class="radio" >
<input type="radio" name="sexe" value="M" id="homme" <c:if test="${empty user || user.isMan}"> checked </c:if>/> Homme
</label>
<label for="femme" class="radio">
    <input type="radio" name="sexe" value="F" id="femme" <c:if test="${!empty user && !user.isMan}"> checked </c:if>/> Femme
</label>

<p>
    <button class="btn btn-primary" type="submit"><i class="icon-white icon-user"></i> Inscription</button>
    <button class="btn btn-custom" type="reset">R�initialiser <i class="icon-refresh"></i></button>
</p>

 <c:if test="${!empty form.result}">
    <span class="label ${empty form.errors ? 'label-success' : 'label-warning'}">${form.result}</span>
</c:if>
