<%-- 
    Document   : inc_navbar
    Created on : 2 janv. 2013, 01:02:57
    Author     : mbit
--%>

        <c:url value='/VisageCreation' var='link2visage'/>
        <c:url value='/Disconnection' var='link2Disconnection'/>
        <c:url value='/index.jsp' var='link2index'/>
        <c:url value='/Connection' var='link2connection'/>
        <c:url value='/UpdateInfo' var='link2update'/>
        <c:url value='/PureeDeVisage' var='link2home'/>

    <nav class="navbar">
	    <div class="navbar-inner">
		<div class="container">
                    <c:choose>
                        <c:when test="${!empty sessionScope.userSession}">
                            <div class="btn-group pull-right">
                                <a class="btn btn-inverse" href="${link2home}"><i class="icon-home icon-white"></i> ${sessionScope.userSession.pseudo}</a>
                                <a class="btn btn-inverse dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="${link2update}"><i class="icon-pencil"></i> Modifier mes informations</a></li>                                      
                                  <li class="divider"></li>
                                  <li><a href="${link2visage}"><i class="icon-cog"></i> Ajouter un visage</a></li>
                                  <li><a href="${link2Disconnection}"><i class="icon-circle-arrow-left"></i> D�connexion</a></li>
                                </ul>
                              </div>
                        </c:when>
                        <c:otherwise>
                            <a class="btn btn-inverse pull-right" href="${link2connection}"><i class="icon-home icon-white"></i> Connexion </a>
                        </c:otherwise>
                    </c:choose>
                
		    <ul class="nav">
			<a class="brand" href="${link2index}">Pur�e de visage</a> 			
			<li class="dropdown"> 
			<a class="dropdown-toggle" data-toggle="dropdown" href="#"> Liens <b class="caret"></b></a>
			<ul class="dropdown-menu">
			    <li><a href="#">Lien1</a></li>
			    <li><a href="#">Lien2</a></li>			    
			    <li class="divider"></li>
			    <li><a href="#">Autres liens</a></li>
			</ul>
			</li>
			<li class="divider-vertical"></li>
			<li> <a href="#">Activit�s</a> </li>
		    </ul>
		</div>
            </div>
    </nav>

