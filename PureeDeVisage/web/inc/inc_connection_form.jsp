<%-- 
    Document   : inc_connection_form
    Created on : 30 d�c. 2012, 08:54:26
    Author     : mbit
--%>

<input type="email" name="email" class="input-medium" value="${cUser.email}" required autofocus placeholder="Email" />
                                
<input type="password" name="motdepasse" class="input-medium pull-right" required placeholder="Mot de passe" />
<c:if test="${!empty cForm.errors.motdepasse || !empty cForm.errors.email}">
    <div class="alert alert-error">
        <button type="button" class="close" >&times;</button>
         ${cForm.errors.motdepasse}<br /> 
         ${cForm.errors.email} 
    </div>
</c:if>
<div class="controls"><br />
    <label for="checking" class="checkbox"> 
    <input type="checkbox" name="remember" class="checkbox" id="checking"/>
    Se souvenir de moi
    </label>
    <button class="btn btn-primary pull-right" type="submit"><i class="icon-white icon-user"></i> Connexion</button>

</div>
<c:if test="${!empty cForm.result}">
    <span class="label ${empty cForm.errors ? 'label-success' : 'label-warning'}">${cForm.result}</span><br />
</c:if>
<%-- V�rification de la pr�sence d'un objet utilisateur en session --%>
<c:if test="${!empty sessionScope.userSession}">
   <%-- Si l'utilisateur existe en session, alors on affiche son adresse email. --%>
   <span class="label label-info">Vous �tes connect�(e): ${sessionScope.userSession.email}</span>
</c:if> 
