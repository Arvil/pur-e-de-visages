<%-- 
    Document   : inc_register_form
    Created on : 30 d�c. 2012, 08:49:06
    Author     : mbit
--%>
<input type="hidden" name="iduser" value="${ sessionScope.userSession.id }"/>
<label for="email">Adresse email </label>
<input type="email" id="email" name="email" placeholder="Email" required value="<c:out value="${sessionScope.userSession.email}"/>" size="20" maxlength="60" />
<c:if test="${!empty form.errors.email}">
    <div class="alert alert-error">
        <button type="button" class="close" >&times;</button>
         ${uform.errors.email} 
    </div>
</c:if>


<label for="motdepasse">Mot de passe</label>
<input type="password" id="motdepasse" name="motdepasse" placeholder="Mot de passe" required  size="20" maxlength="20" />

<label for="confirmation">Confirmation du mot de passe </label>
<input type="password" id="confirmation" required name="confirmation" placeholder="Confirmation mot de passe"  size="20" maxlength="20" />

<c:if test="${!empty uform.errors.motdepasse}">
    <div class="alert alert-error">
        <button type="button" class="close" >&times;</button>
         ${uform.errors.motdepasse} 
    </div>
</c:if>

<label for="nom">Nom d'utilisateur</label>
<input type="text" id="nom" required name="nom" placeholder="Pseudo" value="<c:out value="${sessionScope.userSession.pseudo}"/>" size="20" maxlength="20" />

<c:if test="${!empty uform.errors.nom}">
    <div class="alert alert-error">
        <button type="button" class="close" >&times;</button>
         ${uform.errors.nom}
    </div>
</c:if>


<p>
    <button class="btn btn-primary" type="submit"><i class="icon-white icon-thumbs-up"></i> Modifier </button>
    <button class="btn btn-custom" type="reset">R�initialiser <i class="icon-refresh"></i></button>
</p>

 <c:if test="${!empty uform.result}">
    <span class="label ${empty uform.errors ? 'label-success' : 'label-warning'}">${uform.result}</span>
</c:if>