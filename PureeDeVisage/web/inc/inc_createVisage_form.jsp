<%-- 
    Document   : inc_createVisage_form
    Created on : 1 janv. 2013, 13:27:15
    Author     : arvil
--%>

<input type="hidden" id="owner_email" name="owner_email" value="${sessionScope.userSession.email}" />

<label for="nom">Nom </label>
<input type="text" id="nom" name="nom" placeholder="Nom" required value="<c:out value="${visage.lastName}"/>" size="20" maxlength="60" />
<c:if test="${!empty form.errors.email}">
    <div class="alert alert-error">
        <button type="button" class="close" >&times;</button>
         ${form.errors.nom} 
    </div>
</c:if>


<label for="prenom">Pr�nom </label>
<input type="text" id="prenom" name="prenom" placeholder="Pr�nom" required value="<c:out value="${visage.firstName}"/>" size="20" maxlength="20" />

<label for="photo">Photo </label>

<div class="fileupload fileupload-new" data-provides="fileupload">
    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div><br />
    <span class="btn btn-file"><input type="file" id="photo" required name="photo" placeholder="Choisir une photo" value="" size="20" maxlength="20" accept="image/*" /></span>
</div><br/>

<c:if test="${!empty form.errors.photo}">
    <div class="alert alert-error">
        <button type="button" class="close" >&times;</button>
         ${form.errors.photo} 
    </div>
</c:if>

<p>
    <button class="btn btn-primary" type="submit"><i class="icon-white icon-user"></i> Ajouter</button>
    <button class="btn btn-custom" type="reset">R�initialiser <i class="icon-refresh"></i></button>
</p>

 <c:if test="${!empty form.result}">
    <span class="label ${empty form.errors ? 'label-success' : 'label-warning'}">${form.result}</span>
</c:if>
