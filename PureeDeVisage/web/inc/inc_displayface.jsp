<%-- 
    Document   : inc_displayface
    Created on : 10 janv. 2013, 04:23:29
    Author     : mbit
--%>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                <h3 id="myModalLabel">${face.firstName} ${face.lastName}</h3>
              </div>
              
              <div class="modal-body">
                  <c:url value="/DisplayImage" var="vis"> 
                              <c:param name="ImageName" value="${face.picturePath}"/>
                  </c:url> 
                  <div class="row">
                      <div class="span2">
                          <img src="${vis}" alt="${face.firstName}" title="${face.lastName}" style="width: 150px; height: 120px"/>
                      </div>
                      <div class="span4">
                      <p>
                      <table class="table">
                          <tr><th>Pr�nom</th><td>${face.firstName}</td></tr>
                          <tr><th>Nom</th><td>${face.lastName}</td></tr>
                          <tr><th>Ajout� le</th><td>${face.registrationDate}</td></tr>
                           <tr><th>Favoris</th><td>${face.id}</td></tr>
                              
                      </table>
                      </p>    
                      </div>
                      
                  </div>                 
              </div>
                          
              <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>                
              </div>
</div>