<%-- 
    Document   : inscription
    Created on : 28 d�c. 2012, 17:28:09
    Author     : mbit
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Inscription</title>
        <link rel="shortcut icon" type="image/x-icon" href="<c:url value="/img/favicon.png" />"/>
        <c:url value='/bootstrap/css/bootstrap.min.css' var='link2css'/>
        <link type="text/css" rel="stylesheet" href="${ link2css }" />
    </head>
    
    <body>
        <div class="container">
            <c:import url="inc/inc_navbar.jsp"/>
            <div class="row">
                <div class="span6">
                    <form class="well form-vertical" method="post" action="Registration" id="formulaire">
                        <input type="hidden" name="action" value="form_inscription" />
                        <fieldset>
                            <legend>Inscription</legend>                           
                            <c:import url="inc/inc_register_form.jsp"/>
                           
                        </fieldset>
                    </form>
                </div>
                <div class="span5 offset1">
                        <form class="form-inline well" method="post" action="<c:url value='/Connection' />">
                            <input type="hidden" name="action" value="form_connection" />
                            
                            <fieldset>
                                <legend>Connexion</legend>
                                    <c:import url="inc/inc_connection_form.jsp"/> 
                            </fieldset>
                        </form>
                </div>
            </div>
          
                <c:import url="/inc/footer.jsp"/> 
                            
          </div>
    
                <c:import url="/inc/javascript.jsp"/>
    <script>
         $(function(){
             //fermer les messages d'erreur apr�s 3 secondes
             $("div.alert").show("slow").delay(3000).hide("slow");
             $("span.label.label-info").show("slow").delay(3000).hide("slow");
             $("span.label.label-success").show("slow").delay(3000).hide("slow");
             $("span.label.label-warning").show("slow").delay(3000).hide("slow");


             
         });
    </script>
               
    </body> 

</html>

