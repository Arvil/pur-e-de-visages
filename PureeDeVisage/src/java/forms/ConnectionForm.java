/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package forms;

import dao.DAOException;
import dao.UserDao;
import entities.Utilisateur;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.jasypt.util.password.ConfigurablePasswordEncryptor;

/**
 *
 * @author mbit
 */
public final class ConnectionForm {
    
    private static final String ALGO_CHIFFREMENT = "SHA-256";
    private static final String CHAMP_EMAIL  = "email";
    private static final String CHAMP_PASS   = "motdepasse";
    private static final String GLOBALCONNEXION   = "globalconnexion";
    private String motDePasseChiffre;
    private String              result;
    private Map<String, String> errors      = new HashMap<String, String>();
    private UserDao      userDao;

    public ConnectionForm(UserDao userDao) {
        this.userDao = userDao;
    }

    
    public String getResult() {
        return result;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public Utilisateur connectUser( HttpServletRequest request ) {
        
            /* Récupération des champs du formulaire */
            String email = getValue( request, CHAMP_EMAIL );
            String motDePasse = getValue( request, CHAMP_PASS );

            Utilisateur user = new Utilisateur();
            try {
                /* Validation du champ email. */
                treatEmail(email,user);

                /* Validation du champ mot de passe. */
                treatPassword(motDePasse,user);

                treatPassAndMail( motDePasse, email );

                
                 /* Initialisation du résultat global de la check. */
                if ( errors.isEmpty() ) {
                    user = userDao.find(email, motDePasseChiffre);
                    result = "Succès de la connexion.";
                } else {
                    result = "Échec de la connexion.";
                }

            } catch (DAOException ex) {
                result = "Échec de la connexion : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
            }

           
            return user;
        
    }
     /** Valide l'adresse email saisie.
     */
    private void checkEmail( String email ) throws FormCheckingException {
        if ( email != null && !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
            throw new FormCheckingException( "Merci de saisir une adresse mail valide." );
        }
    }

    /**
     * Valide le mot de passe saisi.
     */
    private void checkPassword( String motDePasse ) throws FormCheckingException {
        if ( motDePasse != null ) {
            if ( motDePasse.length() < 3 ) {
                throw new FormCheckingException( "Mot de passe doit avoir au moins 3 caractères." );
            }
        } else {
            throw new FormCheckingException( "Merci de saisir votre mot de passe." );
        }
    }
    
    /**
     * Appel à la validation des mots de passe reçus, chiffrement du mot de
     * passe et initialisation de la propriété motDePasse du bean
     */
    private void checkPassAndMail( String motDePasse, String email ) throws FormCheckingException {
               
        ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
        passwordEncryptor.setAlgorithm( ALGO_CHIFFREMENT );
        passwordEncryptor.setPlainDigest( true );
        motDePasseChiffre = passwordEncryptor.encryptPassword( motDePasse );
        
        if ( userDao.find( email, motDePasseChiffre ) == null ){
            throw new FormCheckingException("Identifiants incorrects");
        }
        
    }

    /*
     * Ajoute un message correspondant au champ spécifié à la map des errors.
     */
    private void setError( String champ, String message ) {
        errors.put( champ, message );
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getValue( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }
    
    /**
    * Appel à la validation de l'adresse email reçue    * Appel à la validation de l'adresse email reçue et initialisation de la
    * propriété email du bean
    */
    private void treatEmail( String email, Utilisateur user ) {
       try {
           checkEmail( email );
       } catch ( FormCheckingException e ) {
           setError( CHAMP_EMAIL, e.getMessage() );
       }
       user.setEmail(email);
       
    }
    
    private void treatPassword( String password , Utilisateur user) {
       try {
           checkPassword( password );
       } catch ( FormCheckingException e ) {
           setError( CHAMP_EMAIL, e.getMessage() );
       }
       user.setPassword(password);
       
    }

    /**
     * Appel à la validation des mots de passe reçus, chiffrement du mot de
     * passe et initialisation de la propriété motDePasse du bean
     */
    private void treatPassAndMail( String motDePasse, String email ) {
        
        try{
            checkPassAndMail(motDePasse, email);
        } catch(FormCheckingException e) {
            setError(GLOBALCONNEXION,e.getMessage());
        }        
        
    }
    
}
