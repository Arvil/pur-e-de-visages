
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package forms;

/**
 *
 * @author mbit
 */
import dao.DAOException;
import dao.UserDao;
import entities.Utilisateur;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.jasypt.util.password.ConfigurablePasswordEncryptor;

public final class RegisterForm {
   

    private static final String ALGO_CHIFFREMENT = "SHA-256";
    private static final String CHAMP_EMAIL  = "email";
    private static final String CHAMP_PASS   = "motdepasse";
    private static final String CHAMP_CONF   = "confirmation";
    private static final String CHAMP_NOM    = "nom";
    private static final String CHAMP_SEXE    = "sexe";

    private String              result;
    private Map<String, String> errors      = new HashMap<String, String>();
    
    private UserDao      userDao;

    public RegisterForm( UserDao userDao ) {
        this.userDao = userDao;
    }


    public String getResult() {
        return result;        
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public Utilisateur registerUser( HttpServletRequest request ) {
        
        String email = getValue( request, CHAMP_EMAIL );
        String motDePasse = getValue( request, CHAMP_PASS );
        String confirmation = getValue( request, CHAMP_CONF );
        String nom = getValue( request, CHAMP_NOM );
        boolean sex = getValue( request, CHAMP_SEXE ).equals("M");
        Timestamp date = new Timestamp( System.currentTimeMillis() );
        Utilisateur user = new Utilisateur();

        try{
            treatEmail(email, user);
            treatPassword(motDePasse, confirmation, user);
            treatName(nom, user);
            treatSex(sex,user);
            treatDate(date, user);

            if ( errors.isEmpty() ) {
                userDao.create( user );
                result = "Succès de l'inscription.";
            } else {
                result = "Échec de l'inscription.";
            }
        } catch ( DAOException e ) {
            result = "Échec de l'inscription : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
    }

        return user;
    }
    
    
     /**
     * Valide l'adresse mail saisie.
     */
    private void checkEmail( String email ) throws FormCheckingException {
        if ( email != null && email.trim().length() != 0 ) {
            if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
                throw new FormCheckingException( "Merci de saisir une adresse mail valide." );
            } else if ( userDao.find( email ) != null ) {
                throw new FormCheckingException( "Cette adresse email est déjà utilisée, merci d'en choisir une autre." );
            }
        } else {
            throw new FormCheckingException( "Merci de saisir une adresse mail." );
        }
    }

    /**
     * Valide les mots de passe saisis.
     */
    private void checkPassword( String motDePasse, String confirmation ) throws FormCheckingException{
        if (motDePasse != null && motDePasse.trim().length() != 0 && confirmation != null && confirmation.trim().length() != 0) {
            if (!motDePasse.equals(confirmation)) {
                throw new FormCheckingException("Les mots de passe entrés sont différents, merci de les saisir à nouveau.");
            } else if (motDePasse.trim().length() < 3) {
                throw new FormCheckingException("Les mots de passe doivent contenir au moins 3 caractères.");
            }
        } else {
            throw new FormCheckingException("Merci de saisir et confirmer votre mot de passe.");
        }
    }
    

    /**
     * Valide le nom d'user saisi.
     */
    private void checkName( String nom ) throws FormCheckingException {
        if ( nom != null && nom.trim().length() < 3 ) {
            throw new FormCheckingException( "Le nom d'user doit contenir au moins 3 caractères." );
        }
    }
    
        /*
     * Ajoute un message correspondant au champ spécifié à la map des errors.
     */
    private void setError( String champ, String message ) {
        errors.put( champ, message );
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getValue( HttpServletRequest request, String nomChamp ) {
        String value = request.getParameter( nomChamp );
        if ( value == null || value.trim().length() == 0 ) {
            return null;
        } else {
            return value.trim();
        }        
    }
    
   /**
    * Appel à la validation de l'adresse email reçue    * Appel à la validation de l'adresse email reçue et initialisation de la
    * propriété email du bean
    */
    private void treatEmail( String email, Utilisateur user ) {
       try {
           checkEmail( email );
       } catch ( FormCheckingException e ) {
           setError( CHAMP_EMAIL, e.getMessage() );
       }
       user.setEmail( email );
    }

    /*
     * Appel à la validation des mots de passe reçus, chiffrement du mot de
     * passe et initialisation de la propriété motDePasse du bean
     */
    private void treatPassword( String motDePasse, String confirmation, Utilisateur user ) {
        try {
            checkPassword( motDePasse, confirmation );
        } catch ( FormCheckingException e ) {
            setError( CHAMP_PASS, e.getMessage() );
            setError( CHAMP_CONF, null );
        }

        /*
         * Utilisation de la bibliothèque Jasypt pour chiffrer le mot de passe
         * efficacement.
         * 
         * L'algorithme SHA-256 est ici utilisé, avec par défaut un salage
         * aléatoire et un grand nombre d'itérations de la fonction de hashage.
         * 
         * La String retournée est de longueur 56 et contient le hash en Base64.
         */
        ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
        passwordEncryptor.setAlgorithm( ALGO_CHIFFREMENT );
        passwordEncryptor.setPlainDigest( true );
        String motDePasseChiffre = passwordEncryptor.encryptPassword( motDePasse );

        user.setPassword( motDePasseChiffre );
    }
    
    private void treatName(String nom, Utilisateur user){
        try {
            checkName( nom );
        } catch ( FormCheckingException e ) {
            setError( CHAMP_NOM, e.getMessage() );
        }
        user.setPseudo( nom );
    }
    
    private void treatSex(boolean sex, Utilisateur user){
        user.setIsMan(sex);
    }
    /*
    * Simple initialisation de la propriété dateInscription du bean avec la
    * date courante.
    */
    private void treatDate( Timestamp date, Utilisateur user ) {
        user.setRegistrationDate(date);
    }
}
