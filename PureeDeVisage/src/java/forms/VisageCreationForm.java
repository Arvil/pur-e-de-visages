/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package forms;

import dao.DAOException;
import dao.UserDao;
import dao.VisageDAO;
import entities.Utilisateur;
import entities.Visage;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

/**
 *
 * @author Arvil
 */
public final class VisageCreationForm {
    
    private static final String CHAMP_FIRSTNAME  = "prenom";
    private static final String CHAMP_LASTNAME   = "nom";
    private static final String CHAMP_PICTURE    = "photo";
    private static final String CHAMP_EMAIL      = "owner_email";
    private Map<String, String> errors      = new HashMap<String, String>();
    private String result;
    private VisageDAO      visageDAO;
    private UserDao        userDAO;
       


    public VisageCreationForm(VisageDAO visageDAO,UserDao userDao) {
        this.visageDAO = visageDAO;
        this.userDAO = userDao;
    }
    
    public String getResult() {
        return result;        
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public Visage registerVisage( HttpServletRequest request) throws IOException, ServletException {
        
        
        String lastname = convertStreamToString(request.getPart(CHAMP_LASTNAME).getInputStream());
        String firstname = convertStreamToString(request.getPart(CHAMP_FIRSTNAME).getInputStream());
        
        // Get this ***$^ù*ù* image
        Part filePart = request.getPart(CHAMP_PICTURE);
        
        Timestamp date = new Timestamp( System.currentTimeMillis() );
        Visage v = new Visage();

        try{
            treatLastName(lastname, v);
            treatFirstName(firstname, v);
            treatPhoto(filePart, v);
            treatDate(date, v);
            
            //v.setVotants(new TreeSet<Utilisateur>());
            String owneremail = convertStreamToString(request.getPart(CHAMP_EMAIL).getInputStream());
            Utilisateur u = userDAO.find(owneremail);
            u.addOwnedVisage(v);           
            
            if ( errors.isEmpty() ) {
                visageDAO.create( v );
                result = "Visage ajouté : " + v.getFirstName() + " " + v.getLastName();
            } else {
                // TODO : remove the error
                result = "Échec de l'ajout du visage."; //+ errors.values().iterator().next();
            }
        } catch ( DAOException e ) {
            result = "Échec de l'ajout : une erreur imprévue est survenue, merci de réessayer dans quelques instants.";
    }

        return v;
    }

    /**
     * Valide le prénom ou le nom.
     */
    private void checkFirstOrLastName( String ln ) throws FormCheckingException{
        if (ln == null || ln.trim().length() == 0) {
            throw new FormCheckingException("Champ vide.");
        }
    }
    
     /**
     * Ajoute un message correspondant au champ spécifié à la map des errors.
     */
    private void setError( String champ, String message ) {
        errors.put( champ, message );
    }

    
    
   /**
    * Appel à la validation du prénom reçu
    */
    private void treatFirstName( String firstname, Visage v ) {
       try {
           checkFirstOrLastName( firstname );
       } catch ( FormCheckingException e ) {
           setError( CHAMP_FIRSTNAME, e.getMessage() );
       }
       v.setFirstName( firstname );
    }

    /**
     * Appel à la validation du nom reçu
     */
    private void treatLastName( String lastname, Visage v ) {
        try {
            checkFirstOrLastName( lastname );
        } catch ( FormCheckingException e ) {
            setError( CHAMP_LASTNAME, e.getMessage() );
        }
        v.setLastName(lastname);
    }
    
    /**
     * Validation de la photo reçue
     */
    private void treatPhoto(Part filePart, Visage v) {
        byte[] b;
        b = new byte[(int) filePart.getSize()];
        try {
            InputStream fileContent = filePart.getInputStream();
            BufferedImage bi = ImageIO.read(fileContent);
            
            int i = 0;

            File outputfile = new File(v.getFirstName() + v.getLastName() + ".png");
            // While we don't find a new name for our pic
            while(outputfile.exists()) {
                i++;
                outputfile = new File(v.getFirstName() + v.getLastName() + i + ".png");
            }
            ImageIO.write(bi, "png", outputfile);
            v.setPicturePath(outputfile.getAbsolutePath());
            //File tosave  = new File(chemin, outputfile.getName());
            //ImageIO.write(bi, "png", tosave);
            //v.setPicturePath(tosave.getAbsolutePath());
        } catch (IOException e) {
            setError(CHAMP_PICTURE, e.getMessage());
        } catch(Exception e) {
            setError(CHAMP_PICTURE, "format invalide");
        }
    }
    
    /**
     * @param is le InputStream a convertir
     * @return le string passé par l'inputstream.
     * @throws IOException 
     */
    public String convertStreamToString(InputStream is)
            throws IOException {
        //
        // To convert the InputStream to String we use the
        // Reader.read(char[] buffer) method. We iterate until the
        // Reader return -1 which means there's no more data to
        // read. We use the StringWriter class to produce the string.
        //
        if (is != null) {
            Writer writer = new StringWriter();
 
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(
                        new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {        
            return "";
        }
    } 
     /*
    * Simple initialisation de la propriété dateInscription du bean avec la
    * date courante.
    */
    private void treatDate( Timestamp date, Visage v) {
        v.setRegistrationDate(date);
    }
    
   
}