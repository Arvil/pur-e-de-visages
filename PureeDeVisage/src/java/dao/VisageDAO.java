/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Visage;
import java.util.ArrayList;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author mbit
 */
@Stateless
public class VisageDAO {

    private static final String JPQL_SELECT_BY_NAME = "SELECT v "
            + "                                         FROM Visage v "
            + "                                             WHERE v.firstname=:firstname "
            + "                                                 AND v.lastname=:lastname";
    private static final String JPQL_SEARCH_BY_NAME = "SELECT v FROM Visage v"+
                                                         "WHERE v.firstname LIKE :firstname" +
                                                            " AND v.lastname LIKE :lastname";
    private static final String JPQL_SELECT_ALL  = "SELECT v FROM Visage v";
    private static final String JPQL_SELECT_BY_ID  = "SELECT v FROM Visage v WHERE v.id=:id";
    
    private static final String PARAM_LASTNAME           = "lastname";
    private static final String PARAM_FIRSTNAME           = "firstname";
    private static final String PARAM_ID           = "id";
    

    // Injection du manager, qui s'occupe de la connexion avec la BDD
    @PersistenceContext(unitName = "PureeDeVisagePU")
    private EntityManager       em;

    // Enregistrement d'un nouvel user
    /**
     *
     * @param Visage
     * @throws DAOException
     */
    public void create( Visage v ) throws DAOException {
        try {
            em.persist( v );
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
    }

    // Recherche d'un visage à partir de son nom et son prénom
    /**
     *
     * @param firstname
     * @param lastname
     * @return v un visage dont les nom et prénom correspondent aux paramètres
     * @throws DAOException
     */
    public Visage find( String firstname, String lastname ) throws DAOException {
        Visage v = null;
        Query requete = em.createQuery( JPQL_SELECT_BY_NAME );
        requete.setParameter( PARAM_LASTNAME, lastname );
        requete.setParameter( PARAM_FIRSTNAME, firstname);
        try {
            v = (Visage) requete.getSingleResult();
        } catch ( NoResultException e ) {
            return null;
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
        return v;
    }
    
    /**
     *
     * @return the set of registered users
     */
    public Collection<Visage> findAll(){
        Query requete = em.createQuery( JPQL_SELECT_ALL );
        Collection<Visage> resultat = null;
        try {        
            resultat = (Collection<Visage>)requete.getResultList();
        }catch(DAOException e){
            throw new DAOException(e);            
        }
        return resultat;
    }
    
    /**
     *
     * @param firstname
     * @param lastname
     * @return
     */
    public ArrayList<Visage> search(String firstname, String lastname) {
        ArrayList<Visage> v = null;
        Query requete = em.createQuery( JPQL_SEARCH_BY_NAME );
        requete.setParameter( PARAM_LASTNAME, "%" + lastname + "%");
        requete.setParameter( PARAM_FIRSTNAME, "%" +firstname + "%");
        try {
            v = (ArrayList<Visage>) requete.getResultList();        
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
        return v;
    }

    public Visage find(Long id) {
        Visage v = null;
        Query requete = em.createQuery( JPQL_SELECT_BY_ID );
        requete.setParameter( PARAM_ID, id );
        
        try {
            v = (Visage) requete.getSingleResult();
        } catch ( NoResultException e ) {
            return null;
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
        return v;
    }
}
