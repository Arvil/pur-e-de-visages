/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Utilisateur;
import entities.Visage;
import java.util.Collection;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author mbit
 */
@Stateless
public class UserDao {

   
    private static final String JPQL_SELECT_PAR_EMAIL = "SELECT u FROM Utilisateur u WHERE u.email=:email";
    private static final String JPQL_SELECT_PAR_EMAIL_ET_PASS = "SELECT u "
            + "                                                     FROM Utilisateur u "
            + "                                                     WHERE u.email=:email "
            + "                                                       AND u.password=:password ";
   private static final String JPQL_SELECT_PAR_ID = "SELECT u FROM Utilisateur u WHERE u.id=:id";
   private static final String JPQL_SELECT_ALL =  "SELECT u From Utilisateur u";
   private static final String JPQL_SELECT_VISAGES =  "SELECT v From Visage v WHERE v.owner.id = :id ";


   private static final String PARAM_EMAIL           = "email";
   private static final String PARAM_PASS           = "password";
   private static final String PARAM_ID="id";
   
    // Injection du manager, qui s'occupe de la connexion avec la BDD
    @PersistenceContext(unitName = "PureeDeVisagePU")
    private EntityManager       em;

    // Enregistrement d'un nouvel user
    /**
     *
     * @param user
     * @throws DAOException
     */
    public void create( Utilisateur user ) throws DAOException {
        try {
            em.persist( user );
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
    }

    // Recherche d'un user à partir de son adresse email
    /**
     *
     * @param email
     * @return
     * @throws DAOException
     */
    public Utilisateur find( String email ) throws DAOException {
        Utilisateur user = null;
        Query requete = em.createQuery( JPQL_SELECT_PAR_EMAIL );
        requete.setParameter( PARAM_EMAIL, email);
        try {
            user = (Utilisateur) requete.getSingleResult();
        } catch ( NoResultException e ) {            
            return null;
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
        return user;
    }
    
    /**
     *
     * @param email
     * @param password
     * @return
     * @throws DAOException
     */
    public Utilisateur find( String email, String password ) throws DAOException {
        Utilisateur user = null;
        Query requete = em.createQuery( JPQL_SELECT_PAR_EMAIL_ET_PASS );
        requete.setParameter( PARAM_EMAIL, email );
        requete.setParameter( PARAM_PASS, password );
        try {
            user = (Utilisateur) requete.getSingleResult();
        } catch ( NoResultException e ) {            
            return null;
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
        return user;
    } 
    
    public Utilisateur find(Long id){
        Utilisateur user = null;
        Query requete = em.createQuery( JPQL_SELECT_PAR_ID );
        requete.setParameter( PARAM_ID, id );
        
        try {
            user = (Utilisateur) requete.getSingleResult();
        } catch ( NoResultException e ) {            
            return null;
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
        return user;
        
    }
    
    public Collection<Visage> getOwnedVisages(Long iduser){
        Collection<Visage> resultat = null;
        Query requete = em.createQuery( JPQL_SELECT_VISAGES );
        requete.setParameter( PARAM_ID, iduser );
        try {        
            resultat = (Collection<Visage>)requete.getResultList();
        }catch(DAOException e){
            throw new DAOException(e);            
        }
        return resultat;
    }
    
    /**
     *
     * @param user
     */
    public void delete(Utilisateur user){
        try{
            em.remove(em.merge(user));
        }catch(Exception e ){
            throw new DAOException(e);
        }
    }
    
    /**
     *
     * @param email
     */
    public void updateEmail(String email, long id){
        Utilisateur u = (Utilisateur)em.find(Utilisateur.class , id);
         u.setEmail(email);
         em.persist(u);
        
    }
    
    /**
     *
     * @return the set of registered users
     */
    public Collection<Utilisateur> findAll(){
        Query requete = em.createQuery( JPQL_SELECT_ALL );
        Collection<Utilisateur> resultat = null;
        try {        
            resultat = (Collection<Utilisateur>)requete.getResultList();
        }catch(DAOException e){
            throw new DAOException(e);            
        }
        return resultat;
    }
    
    /**
     *
     * @param password
     */
    public void updatePassword(String password, long id){
         
         Utilisateur u = (Utilisateur)em.find(Utilisateur.class , id);
         u.setPassword(password);
         em.persist(u);
        
    }
    
    
    /**
     *
     * @param pseudo
     */
    public void updatePseudo(String pseudo, long id){
         Utilisateur u = (Utilisateur)em.find(Utilisateur.class , id);
         u.setPseudo(pseudo);
         em.persist(u);
    }
    
}
