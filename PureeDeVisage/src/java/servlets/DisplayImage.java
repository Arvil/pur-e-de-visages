/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter; 
import java.net.URLDecoder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
 
//import org.apache.log4j.Logger;
/**
 *
 * @author mbit
 */
public class DisplayImage extends HttpServlet {
    
    
    private final static int TAILLE_TAMPON= 4096;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DisplayImage</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DisplayImage at " + request.getContextPath() + "</h1>"+request.getPathInfo());
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        
            /*
            * Récupération du chemin du fichier demandé au sein de l'URL de la
            * requête
            */
           String fichierRequis = request.getParameter("ImageName");

            /*
             * Décode le nom de fichier récupéré, susceptible de contenir des
             * espaces et autres caractères spéciaux, et prépare l'objet File
             */
            fichierRequis = URLDecoder.decode( fichierRequis, "UTF-8" );
            File file = new File( fichierRequis );
        
            /* Vérifie que le fichier existe bien et afficher une image par defaut sinon*/
            if ( !file.exists() ) {
                String path = getServletContext().getRealPath("")+"/img/img1.jpg";
                file = new File(path);	
            }
             /* Récupère le type du fichier */
            String type = getServletContext().getMimeType( file.getName() );

            /*
             * Si le type de fichier est inconnu, alors on initialise un type par
             * défaut
             */
            if ( type == null ) {
                type = "application/octet-stream";
            }
            
             /* Initialise la réponse HTTP */
            response.reset();
            response.setBufferSize( TAILLE_TAMPON );
            response.setContentType( type );
            response.setHeader( "Content-Length", String.valueOf( file.length() ) );
            response.setHeader( "Content-Disposition", "inline; filename=\"" + file.getName() + "\"" );
            
            /* Prépare les flux */
            BufferedInputStream entree = null;
            BufferedOutputStream sortie = null;
            try {
                /* Ouvre les flux */
                entree = new BufferedInputStream( new FileInputStream( file ), TAILLE_TAMPON );
                sortie = new BufferedOutputStream( response.getOutputStream(), TAILLE_TAMPON );

                /* Lit le fichier et écrit son contenu dans la réponse HTTP */
                byte[] tampon = new byte[TAILLE_TAMPON];
                int longueur;
                while ( ( longueur = entree.read( tampon ) ) > 0 ) {
                    sortie.write( tampon, 0, longueur );
                }
            } finally {
                try {
                    sortie.close();
                } catch ( IOException ignore ) {
                }
                try {
                    entree.close();
                } catch ( IOException ignore ) {
                }
            }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
