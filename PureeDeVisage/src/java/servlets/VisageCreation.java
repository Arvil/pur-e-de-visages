/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import dao.UserDao;
import dao.VisageDAO;
import entities.Utilisateur;
import entities.Visage;
import forms.VisageCreationForm;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Arvil
 */
@MultipartConfig
public class VisageCreation extends HttpServlet {

    
    public static final String ATT_VISAGE = "visage";
    public static final String ATT_FORM = "form";
    public static final String VIEW = "/restricted/createVisage.jsp";
    public static final String ACTION = "action";
    public static final String CHEMIN        = "chemin";
    
    // Injection de notre EJB (Session Bean Stateless)
    @EJB
    private VisageDAO visageDAO;
    @EJB
    private UserDao userDAO;
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Inscription</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Inscription at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher( VIEW ).forward( request, response );

    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {     
        
            //String chemin = this.getServletConfig().getInitParameter( CHEMIN );
            //String chemin = getServletContext().getRealPath("/")+"img";

            /* Préparation de l'objet formulaire */
            VisageCreationForm form = new VisageCreationForm(visageDAO,userDAO);

            /* Appel au traitement et à la validation de la requête, et récupération du bean en résultant */
            
            Visage v = form.registerVisage( request); //, chemin );
             
            /* Stockage du formulaire et du bean dans l'objet request */
            request.setAttribute( ATT_FORM, form );
            request.setAttribute( ATT_VISAGE, v );             
           
            /* Transmission de la paire d'objets request/response à notre JSP */
            this.getServletContext().getRequestDispatcher( VIEW ).forward( request, response );       

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    
     
}
